# PDF-weave

A simple python script to weave together two separate PDF files.

The intended usage scenario is when you have a double-side document
that you scan from a scanner with an automatic document feeder (ADF),
BUT the it lacks the duplex function
(i.e., it cannot automatically scan both sides of the document).

The script enables you to do the following:

1. Using the ADF, scan the odd-numbered pages to one PDF (`odd.pdf`)
2. Turn pile of paper over and scan the even-numbered pages
(in reverse order) to another PDF (`even.pdf`)
3. Run this script to "weave" the pages together and create the complete PDF.


## Prerequisites and Installation

_Note: This script (and instructions here) are minimally tested on OSX only._

This script requires dependent python modules to run.
To install them, run the following commands in the terminal:

```bash
sudo easy_install pip
sudo pip install -r requirements.txt
```
## Usage

```
python pdf-weave.py odd.pdf even.pdf
```

**IMPORTANT NOTE:** The pages in `even.pdf` should be in reverse order.
That is, if there are nine pages in the combined PDF,
the page ordering of `even.pdf` should be page 8, 6, 4, 2.

The combined image is always saved as `weaved.jpg`.
