import io
import os
from PyPDF2 import PdfFileReader, PdfFileWriter
import sys


file_names = sys.argv[1:]
if len(file_names) != 2:
    print("Must provide exactly two PDF files to weave.")
    sys.exit(1)

for f in file_names:
    if not (os.path.isfile(f) and os.access(f, os.R_OK)):
        print "File '%(fn)s' does not exist." % { "fn": f }
        sys.exit(2)

odd_pages_reader = PdfFileReader(file_names[0])
even_pages_reader = PdfFileReader(file_names[1])
# Note: It is assumed that the even pages are in reverse order!!

odd_page_count = odd_pages_reader.getNumPages()
even_page_count = even_pages_reader.getNumPages()
odd_even_page_count_diff = odd_page_count - even_page_count

if odd_even_page_count_diff < 0:
    print "There are more even pages than odd pages."
    sys.exit(3)

if odd_even_page_count_diff > 1:
    print "There are too many odd pages compared to number of even pages."
    sys.exit(4)

writer = PdfFileWriter()

for i in range(0, odd_page_count):
    writer.addPage(odd_pages_reader.getPage(i))

for i in range(0, even_page_count):
    page_to_get = even_page_count - i - 1;
    insert_at_index = (i * 2) + 1
    writer.insertPage(even_pages_reader.getPage(page_to_get), insert_at_index)

stream = io.open("weaved.pdf", mode='w+b')
writer.write(stream)
stream.close()
